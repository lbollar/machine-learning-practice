import pandas as pd
from matplotlib import pyplot as plt
import numpy as np

x = pd.read_table("C:/Users/lollar/Downloads/ex2x.dat",header=None, names=["Age"])
y = pd.read_table("C:/Users/lollar/Downloads/ex2y.dat", header=None)

plt.clf()
plt.axis([0,9,0,1.4])
plt.plot(x,y,'o')

X = np.ones((50,1))
X = np.append(X,x,1)
y = np.array(y)

X = np.matrix(X)
y = np.matrix(y)

m = len(X)

theta = np.matrix([[0.,0.]])
alpha = 0.07

# Batch Gradient Descent
for n in range(1500):
    grad = (1/m) * X.T * (X * theta.T - y)
    theta = theta - alpha * grad.T

# Stochastic Gradient Descent
theta = np.matrix([[0., 0.]])
thetaTemp = np.matrix([[0., 0.]])
for n in range(500):
    for i in range(50):
        for j in range(2):
            thetaTemp[0,j] = theta[0,j] + alpha * ( y[i,0] -  X[i,:] * theta.T ) * X[i,j]
        theta = theta + thetaTemp
        print(theta)
        print(thetaTemp)

        
plt.clf()
plt.axis([1.7, 8.2, 0.7, 1.4])
plt.plot(x,y,'o')
plt.plot(X[:,1], X*theta.T)

