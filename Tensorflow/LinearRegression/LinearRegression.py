import tensorflow as tf
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

x = np.linspace(0,10,200).reshape((200,-1))
y_data = 2 * x + np.random.normal(size=(200,1)) + 2


X = tf.placeholder(tf.float32)

W = tf.Variable(tf.random_uniform([1], -1.0, 1.0))
b = tf.Variable(tf.zeros([1]))

y = tf.add(tf.mul(W,X),b)


loss = tf.reduce_mean(tf.square(y - y_data))

optimizer = tf.train.GradientDescentOptimizer(0.01)

train = optimizer.minimize(loss)

init = tf.initialize_all_variables()

# Launch graph
sess = tf.Session()
sess.run(init)                                

# Fit regression 
for i in range(200):
  x = np.matrix(x)
  sess.run(train, feed_dict={X:x})
  if i % 20 == 0:
        print(sess.run(W), sess.run(b))  
    
plt.plot(x,y_data,'o')

ylim = 10 * sess.run(W) + sess.run(b)
intercept = sess.run(b)

plt.plot([0, 10], [intercept, ylim])

sess.close()


## Multivariate Linear Regression

X = np.asarray(pd.read_csv("PortlandX.csv")).astype('float32')
y_data = np.asarray(pd.read_csv("Portlandy.csv")).astype('float32')

X_mean = np.mean(X,0)
X_std = np.std(X,0)

X = (X - X_mean) / X_std

m = 47

W = tf.Variable(tf.zeros([2,1]))
b = tf.Variable(tf.zeros([1]))

b = tf.Print(b, [b], "Bias: ")
W = tf.Print(W, [W], "Weights: ")

y = tf.add( tf.matmul(X,W), b)
y = tf.Print(y, [y], "y: ")

loss = tf.reduce_mean(tf.square(y - y_data)) / 2
loss = tf.Print(loss, [loss], "loss: ")
optimizer = tf.train.GradientDescentOptimizer(1.)

train = optimizer.minimize(loss)

init = tf.initialize_all_variables()

sess = tf.Session()
sess.run(init)          

J = np.zeros((100,1))                      

for i in range(100):
  sess.run(train)
  J[i,0] = sess.run(loss)
  #if i % 20 == 0:
        #print(sess.run(W), sess.run(b))  

sess.close()

# Cost Function
plt.plot(J)